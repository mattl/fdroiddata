Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/yuriykulikov/AlarmClock
Issue Tracker:https://github.com/yuriykulikov/AlarmClock/issues

Name:Simple Alarm Clock
Auto Name:Simple Alarm Clock
Summary:Improved alarm clock
Description:
Smart alarm clock that includes many improvements over the stock alarm clock.
It does not have any extra features such
as dock mode or a world clock.

[https://github.com/yuriykulikov/AlarmClock/commits/develop All changes]
.

Repo Type:git
Repo:https://github.com/yuriykulikov/AlarmClock.git

Build:2.4.14,2414
    commit=2.4.14
    srclibs=AndroidUtils@78f987a660,ACRA-Yuri@acra-4.4.0
    prebuild=cp -r $$AndroidUtils$$/src/com/github src/com/ && \
        cp -r $$ACRA-Yuri$$/src/main/java/org src/

Build:2.5.07,2507
    commit=2.5.07
    srclibs=AndroidUtils@78f987a660,ACRA-Yuri@2ef9a1cac2106c0e862a42
    prebuild=cp -r $$AndroidUtils$$/src/com/github src/com/ && \
        cp -r $$ACRA-Yuri$$/src/main/java/org src/

Build:2.5.09,2509
    commit=2.5.09
    srclibs=AndroidUtils@78f987a660,ACRA-Yuri@2ef9a1cac2106c0e862a42
    prebuild=cp -r $$AndroidUtils$$/src/com/github src/com/ && \
        cp -r $$ACRA-Yuri$$/src/main/java/org src/

Build:2.7.02,2702
    commit=2.7.02
    srclibs=AndroidUtils@78f987a660,ACRA-Yuri@2ef9a1cac2106c0e862a42
    prebuild=cp -r $$AndroidUtils$$/src/com/github src/com/ && \
        cp -r $$ACRA-Yuri$$/src/main/java/org src/

Build:2.7.05,2705
    commit=2.7.05
    srclibs=AndroidUtils@329ec92b62c5f711ff33d1cc7c959f8e5ebbc553,ACRA-Yuri@2ef9a1cac2106c0e862a42
    prebuild=cp -r $$AndroidUtils$$/src/com/github src/com/ && \
        cp -r $$ACRA-Yuri$$/src/main/java/org src/

Build:2.7.07,2707
    commit=2.7.07
    srclibs=AndroidUtils@329ec92b62c5f711ff33d1cc7c959f8e5ebbc553,ACRA-Yuri@2ef9a1cac2106c0e862a42
    prebuild=cp -r $$AndroidUtils$$/src/com/github src/com/ && \
        cp -r $$ACRA-Yuri$$/src/main/java/org src/

Build:2.8.02,2802
    commit=2.8.02
    srclibs=AndroidUtils@49901275e508d0cf6a93af2ad17a86ccdc34bb43,ACRA@183dbe160ee807f006d739d1b958eb37763ab17a
    prebuild=cp -r $$AndroidUtils$$/src/com/github src/com/ && \
        cp -r $$ACRA$$/src/main/java/org src/

Build:2.9.01,2901
    commit=2.9.01
    gradle=yes
    disable=https://github.com/yuriykulikov/AlarmClock/issues/220
    rm=libs/*

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.9.01
Current Version Code:2901

