Categories:System
License:Apache2
Web Site:http://blog.nagopy.com
Source Code:https://github.com/75py/DisableManager
Issue Tracker:https://github.com/75py/DisableManager/issues

Auto Name:Disable Manager
Summary:Assists the disabling of pre-installed apps
Description:
Assists in the disabling of pre-installed apps. You can show lists of apps
installed apps and can disable/enable them. Please do at your own risk.

[https://translate.google.com/translate?sl=ja&tl=en&u=https%3A%2F%2Fgithub.com%2F75py%2FDisableManager%2Fcommits%2Fmaster All changes (translated by Google)]
.

Repo Type:git
Repo:https://github.com/75py/DisableManager.git

Build:2.0.2,20002
    commit=e7ffb0ffaabdcfd98eed404af34639cc21e3f567
    subdir=app
    gradle=yes
    rm=uiautomator,libs
    prebuild=echo sdk.dir=$$SDK$$ >> ../ViewPagerIndicator/local.properties && \
        sed -i -e "/include ':uiautomator'/d" ../settings.gradle

Build:2.0.3,20003
    commit=2.0.3
    subdir=app
    gradle=yes
    rm=uiautomator,libs
    prebuild=echo sdk.dir=$$SDK$$ >> ../ViewPagerIndicator/local.properties && \
        sed -i -e "/include ':uiautomator'/d" ../settings.gradle

Build:2.0.4,20004
    commit=2.0.4
    subdir=app
    gradle=yes
    rm=uiautomator,libs
    prebuild=echo sdk.dir=$$SDK$$ >> ../ViewPagerIndicator/local.properties && \
        sed -i -e "/include ':uiautomator'/d" ../settings.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.0.4
Current Version Code:20004

