Categories:Internet
License:FreeBSD
Web Site:
Source Code:https://github.com/SamWhited/OpenShareLocationPlugin/
Issue Tracker:https://github.com/SamWhited/OpenShareLocationPlugin//issues

Auto Name:Open Share Location Plugin
Summary:Easy share your location in Conversations
Description:
This plugin for [[eu.siacs.conversations]] allows you to easily share your
location in a chat.
.

Repo Type:git
Repo:https://github.com/SamWhited/OpenShareLocationPlugin/

Build:1.1.1,3
    commit=v1.1.1
    gradle=yes

Build:1.1.2,4
    commit=v1.1.2
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.2
Current Version Code:4

