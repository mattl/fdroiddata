Categories:Multimedia
License:Apache2
Web Site:http://yhcting.tistory.com
Source Code:https://github.com/yhcting/netmbuddy
Issue Tracker:https://github.com/yhcting/netmbuddy/issues

Auto Name:NetMBuddy
Summary:Music player for Youtube
Description:
This basically a Youtube viewer but it's presented like a regular
music player, allowing you to manage queues and playlists easily and
play and pause audio in the background.
.

Repo Type:git
Repo:https://github.com/yhcting/netmbuddy.git

Build:1.2.11-1,29
    commit=netmbuddy-v1.2.11-1

Build:1.2.13,31
    commit=8dc6e0a09b8d28b16c38ff7c2dcd15eff3fbc813

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.13
Current Version Code:31

