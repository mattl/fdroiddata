Categories:Games
License:Apache2
Web Site:http://solitairecg.sourceforge.net
Source Code:http://sourceforge.net/p/solitairecg/code
Issue Tracker:http://solitairecg.sourceforge.net/issues.php

Auto Name:SolitaireCG
Summary:Solitaire Card Games
Description:
SolitaireCG is an adaptation of Ken Magic's "[[com.kmagic.solitaire]]"
for devices with few hardware buttons.

See [http://solitairecg.sourceforge.net/news.php NEWS] for changes
against the original code, and project updates.

The changes improve playability and enable card dealing spider. The
original card graphics, which can scale to higher resolutions, have
been retained.

Solitaire Card Games include Klondike (Regular solitaire), Spider
solitaire, Freecell, Forty Thieves, and variations of these games.
.

Repo Type:git
Repo:git://git.code.sf.net/p/solitairecg/code

Build:1.14,452
    commit=SolitaireCG-1.14

Build:1.15,453
    commit=SolitaireCG-1.15

Build:1.15.1,454
    commit=SolitaireCG-1.15.1

Auto Update Mode:Version SolitaireCG-%v
Update Check Mode:Tags
Current Version:1.15.1
Current Version Code:454

