Categories:Phone & SMS
License:GPLv3
Source Code:https://github.com/SMSSecure/SMSSecure
Issue Tracker:https://github.com/SMSSecure/SMSSecure/issues

Auto Name:SMSSecure
Summary:Send encrypted text messages (SMS)
Description:
Communicatie with friends securely by encrypting your private text messages.
This is a fork of TextSecure, in which SMS encryption is not available anymore.

SMSSecure focuses on SMS. This fork aims to:

* Keep SMS encryption
* Drop Google services dependencies: push messages are not available in SMSSecure; if you want to keep them, SMSSecure is not for you.

If you want to migrate from TextSecure to SMSSecure:

* In TextSecure, export plaintext backup. Warning: the backup will not be encrypted.
* Rename TextSecurePlaintextBackup.xml to SMSSecurePlaintextBackup.xml.
* Install SMSSecure.
* In SMSSecure, import plaintext backup.
* Enjoy SMSSecure!
.

Repo Type:git
Repo:https://github.com/SMSSecure/SMSSecure

Build:0.3.3,6
    disable=smil
    commit=66367479a4f57f347b5cbe8f6f8f632adaae7727
    gradle=yes
    rm=libs/*
    srclibs=GradleWitness@10f1269c0aafdc1d478efc005ed48f3a47d44278,PreferenceFragment@717a45433b927d2f0dfc5328f79e77c9682c37bc,ShortcutBadger@3815ce2ec0c66acd7d7c0b4f2479df8fa70fed87
    prebuild=touch signing.properties && \
        pushd $$GradleWitness$$ && \
        gradle jar && \
        popd && \
        cp $$GradleWitness$$/build/libs/GradleWitness.jar libs/gradle-witness.jar && \
        sed -i -e '20,22d' build.gradle && \
        pushd $$PreferenceFragment$$ && gradle uploadArchives && popd && \
        sed -i -e '/5470f5872514a6226fa1fc6f4e000991f38805691c534cf0bd2778911fc773ad/d' build.gradle

#Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.3.3
Current Version Code:6

